@extends('front.app')

@section('title', $data->title)
@push('meta')
    <meta name="keywords" content="{{$data->tags}}">
    <meta name="description" content="{{$data->description}}">
    <script src="https://cdn.bootcss.com/Swiper/4.3.0/js/swiper.min.js"></script>
@endpush
@push('link')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.bootcss.com/Swiper/4.3.0/css/swiper.min.css" rel="stylesheet">
@endpush
@push('script')
    <script type="text/javascript">
        (function () {
            var appid = 'cytHd60Eg';
            var conf = 'prod_35cd5d68a5ddee61df21a298b6368022';
            var width = window.innerWidth || document.documentElement.clientWidth;
            if (width < 960) {
                window.document.write('<script id="changyan_mobile_js" charset="utf-8" type="text/javascript" src="https://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=' + appid + '&conf=' + conf + '"><\/script>');
            } else {
                var loadJs = function (d, a) {
                    var c = document.getElementsByTagName("head")[0] || document.head || document.documentElement;
                    var b = document.createElement("script");
                    b.setAttribute("type", "text/javascript");
                    b.setAttribute("charset", "UTF-8");
                    b.setAttribute("src", d);
                    if (typeof a === "function") {
                        if (window.attachEvent) {
                            b.onreadystatechange = function () {
                                var e = b.readyState;
                                if (e === "loaded" || e === "complete") {
                                    b.onreadystatechange = null;
                                    a()
                                }
                            }
                        } else {
                            b.onload = a
                        }
                    }
                    c.appendChild(b)
                };
                loadJs("https://changyan.sohu.com/upload/changyan.js", function () {
                    window.changyan.api.config({appid: appid, conf: conf})
                });
            }
        })();
    </script>
    <script>
        window.addEventListener("load", function () {
            var mySwiper = new Swiper('.swiper-container', {
                loop: true,

                // 如果需要分页器
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '">' + (index + 1) + '</span>';
                    },
                },
                lazy: {
                    loadPrevNext: true,
                },
                speed: 1000,
                autoplay: {
                    delay: 2000,
                    stopOnLastSlide: false,
                    disableOnInteraction: true,
                },
                // 如果需要前进后退按钮
                // navigation: {
                //   nextEl: '.swiper-button-next',
                //   prevEl: '.swiper-button-prev',
                // },

                // 如果需要滚动条
                scrollbar: {
                    el: '.swiper-scrollbar',
                },
            })
        });
    </script>
@endpush
@section('content')
    <style>
        .swiper-container {
            width: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .swiper-pagination-bullet {
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            font-size: 12px;
            color: #000;
            opacity: 1;
            background: rgba(0, 0, 0, 0.2);
        }

        .swiper-pagination-bullet-active {
            color: #fff;
            background: #007aff;
        }
    </style>
    <div class="row">
        <div class="col-lg-3 order-lg-1 mb-4 hidden-xs hidden-sm">
            <a href="#" class="btn btn-block btn-primary mb-6">
                扫码领红包
            </a>
            <div class="card">
                <div class="d-flex flex-column">
                    <img src="http://admin.soscoon.com/uploadImages/285b663408cd50e35daface5d61b6395326f32ff.PNG" alt="红包领取" />
                </div>
            </div>
        <!-- Getting started -->
            <div class="list-group list-group-transparent mb-0">
                <a class="list-group-item list-group-item-action active"><span class="icon mr-3"><i
                                class="fe fe-flag"></i></span>热门推荐</a>
            </div>
            <!-- Components -->
            <div class="list-group list-group-transparent mb-0">
                @foreach($hotData as $item)
                    @if ($item->type_value == 'article')
                        <a href="{{asset('/article/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @elseif ($item->type_value == 'ppt')
                        <a href="{{asset('/ppt_template/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @else
                        <a href="{{asset('/html_template/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card">
            @if($data->slider)
                <!-- 轮播图 -->
                    <div class="swiper-container" style="width:100%">
                        <div class="swiper-wrapper">
                            @foreach($images as $image)
                                @if (!$loop->first)
                                    <div class="swiper-slide">
                                        <img src={{asset('/storage/images/'.$image->image_url)}} alt="ppt展示"/>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <!-- 如果需要分页器 -->
                        <div class="swiper-pagination"></div>

                        <!-- 如果需要导航按钮 -->
                        <!-- <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div> -->
                    </div>
                    <!-- 轮播图 -->
                @endif
                <div class="card-body">
                    <div class="text-wrap p-lg-6 fr-view">
                        {!! $data->html_content !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-6" style="padding: 0 0 20px 0">
                @if ($next && $next->type_value == 'article')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/article/'.$next->id)}}"><i class="fe fe-arrow-left"></i>{{ $next->title }}
                        </a>
                    </button>
                @elseif ($next && $next->type_value == 'ppt')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/ppt_template/'.$next->id)}}"><i
                                    class="fe fe-arrow-left"></i>{{ $next->title }}</a>
                    </button>
                @elseif ($next && $next->type_value == 'html')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/html_template/'.$next->id)}}"><i
                                    class="fe fe-arrow-left"></i>{{ $next->title }}</a>
                    </button>
                @endif
            </div>
            <div class="col-xs-6" style="padding: 0 0 20px 0">
                @if ($pre && $pre->type_value == 'article')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/article/'.$pre->id)}}">{{ $pre->title }}<i class="fe fe-arrow-right"></i>
                    </button></a>
                    </button>
                @elseif ($pre && $pre->type_value == 'ppt')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/ppt_template/'.$pre->id)}}">{{ $pre->title }}<i
                                    class="fe fe-arrow-right"></i></a>
                    </button>
                @elseif ($pre && $pre->type_value == 'html')
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/html_template/'.$pre->id)}}">{{ $pre->title }}<i
                                    class="fe fe-arrow-right"></i></a>
                    </button>
                @endif
            </div>
            <div id="SOHUCS" sid="{{$data->alpha}}"></div>
        </div>
    </div>
@endsection
