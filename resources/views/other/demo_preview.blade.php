<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="{{$data->type}}">
    <meta name="description" content="{{$data->description}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('/assets/favicon.ico')}}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/assets/favicon.ico')}}"/>
    <title>{{$data->title}}</title>
</head>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b6693eacab15fc17ab94ccbfecadfa2e";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    iframe {
        border: none;
        overflow: hidden;
        position: absolute;
    }

    #by {
        display: none !important;
    }
</style>
<body>
<iframe id="iframe" src="{{$data->demo_url}}" width="100%" marginwidth="0" frameborder="no"></iframe>
</body>
<script>
    window.onload = function () {
        var _iframe = document.getElementById('iframe');
        _iframe.style.height = window.innerHeight + "px";  //修改样式
    }
    window.onresize = function () {
        var _iframe = document.getElementById('iframe');
        _iframe.style.height = window.innerHeight + "px";  //修改样式
    }
</script>
<!-- 谷歌广告验证 -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5513119698507366",
        enable_page_level_ads: true
    });
</script>
<!-- 谷歌广告验证 -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122460879-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-122460879-1');
</script>
</html>