@extends('auth.common')
@section('title', '登录')

@section('content')
<form class="card" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
                <div class="card-body p-6">
                  <div class="card-title">登录账号</div>
                  <div class="form-group">
                    <label class="form-label">邮箱</label>
                    <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="邮箱账号">
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      密码
                      <a href="./forgot-password.html" class="float-right small">忘记密码?</a>
                    </label>
                    <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="exampleInputPassword1" placeholder="密码">
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} />
                      <span class="custom-control-label">记住我</span>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">登录</button>
                  </div>
                </div>
              </form>
              <div class="text-center text-muted">
                还没有账号? <a href="/register">注册</a>
              </div>
@endsection
