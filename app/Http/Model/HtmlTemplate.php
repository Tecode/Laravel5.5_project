<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class HtmlTemplate extends Model
{
    protected $table = 'html_info';
    public $timestamps = false;
}
