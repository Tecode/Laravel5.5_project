@extends('front.app')

@section('title', $data->title)
@push('meta')
    <meta name="keywords" content="{{$data->tags}}">
    <meta name="description" content="{{$data->description}}">
@endpush
@push('script')
    <script type="text/javascript">
        (function () {
            var appid = 'cytHd60Eg';
            var conf = 'prod_35cd5d68a5ddee61df21a298b6368022';
            var width = window.innerWidth || document.documentElement.clientWidth;
            if (width < 960) {
                window.document.write('<script id="changyan_mobile_js" charset="utf-8" type="text/javascript" src="https://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=' + appid + '&conf=' + conf + '"><\/script>');
            } else {
                var loadJs = function (d, a) {
                    var c = document.getElementsByTagName("head")[0] || document.head || document.documentElement;
                    var b = document.createElement("script");
                    b.setAttribute("type", "text/javascript");
                    b.setAttribute("charset", "UTF-8");
                    b.setAttribute("src", d);
                    if (typeof a === "function") {
                        if (window.attachEvent) {
                            b.onreadystatechange = function () {
                                var e = b.readyState;
                                if (e === "loaded" || e === "complete") {
                                    b.onreadystatechange = null;
                                    a()
                                }
                            }
                        } else {
                            b.onload = a
                        }
                    }
                    c.appendChild(b)
                };
                loadJs("https://changyan.sohu.com/upload/changyan.js", function () {
                    window.changyan.api.config({appid: appid, conf: conf})
                });
            }
        })();
    </script>
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-3 order-lg-1 mb-4 hidden-xs hidden-sm">
            <a href="#" class="btn btn-block btn-primary mb-6">
                扫码领红包
            </a>
            <div class="card">
                <div class="d-flex flex-column">
                    <img src="http://admin.soscoon.com/uploadImages/285b663408cd50e35daface5d61b6395326f32ff.PNG" alt="红包领取" />
                </div>
            </div>
        <!-- Getting started -->
            <div class="list-group list-group-transparent mb-0">
                <a class="list-group-item list-group-item-action active"><span class="icon mr-3"><i
                                class="fe fe-flag"></i></span>热门推荐</a>
            </div>
            <!-- Components -->
            <div class="list-group list-group-transparent mb-0">
                @foreach($hotData as $item)
                    @if ($item->type_value == 'article')
                        <a href="{{asset('/article/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @elseif ($item->type_value == 'ppt')
                        <a href="{{asset('/ppt_template/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @else
                        <a href="{{asset('/html_template/'.$item->id)}}" target="_blank"
                           class="list-group-item list-group-item-action"><span class="icon mr-3"><i
                                        class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <div class="text-wrap p-lg-6 fr-view">
                        <h3 class="mt-0 mb-4 text-center">{{$data->title}}</h3>
                        <p class="text-center">{{$data->description}}</p>
                        </br>
                        <img style="margin: 0 auto; display: inherit;"
                             src="{{asset('/storage/images/html/'.$data->local_url)}}"/>
                        </br>
                        </br>
                        </br>
                        <h4>下载链接</h4>
                        <div class="row">
                            @foreach(explode(',', $data->download_url) as $link)
                                @if ($loop->iteration < 13)
                                    <div style="padding-bottom:5px" class="col-xs-12 col-md-4">
                                        <a style="width: 100%;padding: 8px;border-radius: 0;" href="{{$link}}"
                                           class="btn btn-outline-primary btn-sm">下载链接{{$loop->iteration}}</a>
                                    </div>
                                @endif
                            @endforeach
                            <div style="padding-bottom:5px" class="col-xs-12 col-md-4">
                                <a style="width: 100%;padding: 8px;border-radius: 0;" target="_blank"
                                   href="{{'http://demo.soscoon.com/html_preview/'.$data->id}}"
                                   class="btn btn-outline-primary btn-sm">在线预览</a>
                            </div>
                        </div>
                        <h4>特别说明：</h4>
                        <ul>
                            <li>本站所有资源仅供学习与参考，请勿用于商业用途，否则产生的一切后果将由您自己承担！</li>
                            <li>本站提供的普通下载点可能造成多线程类的软件无法下载，建议直接点击或另存为下载！</li>
                            <li>如有侵犯您的版权，请及时联系283731869@qq.com或者在下方留言，我们将尽快处理。</li>
                        </ul>
                    </div>
                </div>
            </div>
            @if($next)
                <div class="col-xs-6" style="padding: 0 0 20px 0">
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/html/'.$next->id)}}"><i class="fe fe-arrow-left"></i>{{ $next->title }}</a>
                    </button>
                </div>
            @endif
            @if($pre)
                <div class="col-xs-6" style="padding: 0 0 20px 0">
                    <button class="btn btn-primary btn-block next_pre">
                        <a href="{{asset('/html/'.$pre->id)}}">{{ $pre->title }}<i class="fe fe-arrow-right"></i></a>
                    </button>
                </div>
            @endif
            <div id="SOHUCS" sid="{{'YsGDWJLsdwpLO_HTML'.$data->id}}"></div>
        </div>
    </div>
@endsection
