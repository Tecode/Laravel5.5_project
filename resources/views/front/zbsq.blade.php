@extends('front.app')

@section('title', $data->title)
@push('meta')
<meta name="keywords" content="{{$data->title}}">
<meta name="description" content="{{$data->title}}">
@endpush
@push('script')
<script type="text/javascript">
    (function () {
        var appid = 'cytHd60Eg';
        var conf = 'prod_35cd5d68a5ddee61df21a298b6368022';
        var width = window.innerWidth || document.documentElement.clientWidth;
        if (width < 960) {
            window.document.write('<script id="changyan_mobile_js" charset="utf-8" type="text/javascript" src="https://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=' + appid + '&conf=' + conf + '"><\/script>');
        } else {
            var loadJs = function (d, a) {
                var c = document.getElementsByTagName("head")[0] || document.head || document.documentElement;
                var b = document.createElement("script");
                b.setAttribute("type", "text/javascript");
                b.setAttribute("charset", "UTF-8");
                b.setAttribute("src", d);
                if (typeof a === "function") {
                    if (window.attachEvent) {
                        b.onreadystatechange = function () {
                            var e = b.readyState;
                            if (e === "loaded" || e === "complete") {
                                b.onreadystatechange = null;
                                a()
                            }
                        }
                    } else {
                        b.onload = a
                    }
                }
                c.appendChild(b)
            };
            loadJs("https://changyan.sohu.com/upload/changyan.js", function () {
                window.changyan.api.config({appid: appid, conf: conf})
            });
        }
    })();
</script>
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-5 order-lg-1 mb-4">
            <div class="card col-lg-12" style="padding: 1.5em 1em">
                <h4 class="mt-0 mb-4 text-center">{{$data->title}}</h4>
                <form action="{{asset('/zbsq/'.$data->id)}}" method="POST">
                    {{ csrf_field() }}
                    @foreach(explode(' ', $data['artifact_text']) as $index => $name)
                        <div class="form-group">
                            <label class="form-label">{{$name}}</label>
                            <input type="text" class="form-control" name="{{'txt'.$index}}" placeholder="{{$name}}">
                        </div>
                    @endforeach
                    <button class="btn btn-primary btn-block">生成图片</button>
                </form>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <div class="text-wrap p-lg-6 fr-view">
                        <img style="margin: 0 auto; display: inherit;"
                             src="{{asset($image)}}"/>
                        <h4>保存图片方法：</h4>
                        <ul>
                            <li>电脑 : 右键图片-图片另存为-可以存到你的电脑上。</li>
                            <li>手机 : 长按图片-保存图片-可以保存到你的手机里。</li>
                            <li>本网站资源均来自网络，如果侵犯了你的权益请联系我们处理！</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="SOHUCS" sid="{{'cdrDWJLsdDFSdsLO_UTG'.$data->id}}"></div>
        </div>
    </div>
@endsection
