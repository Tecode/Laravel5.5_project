<?php

namespace App\Http\Controllers\Api;

use App\Http\Model\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Model\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    // 保存图片公共方法
    public function saveImage($imageData, $article_id, $type, $type_value) {
        $insert = true;
        if ($type == 'update') {
            foreach($imageData as $image) {
                if ($image == 'undefined') {
                    return true;
                }
                $search = Images::where([
                    'article_id' => $article_id,
                    'image_url'=> $image
                    ])->count();
                if ($search < 1) {
                    $insert = Images::insert([
                        'article_id' => $article_id,
                        'image_url' => $image,
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
        } else {
            foreach($imageData as $image) {
                if ($image == 'undefined') {
                    return true;
                }
                $insert = Images::insert([
                    'article_id' => $article_id,
                    'image_url' => $image,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }
        return $insert;

    }
    // 获取图片方法
    public function getImage($article_id) {
        return (Images::where(['article_id' => $article_id]));
    }
    // 验证数据是否正确
    public function validator($inputArr, $request) {
        $rule = [
            'title' => 'required|min:2',
            'description' => 'required|min:4',
            'htmlContent' => 'required|min:4',
            'tags'=> 'required|min:2',
        ];
        $customInformation = [
            'title.required' => '标题不能为空',
            'title.min' => '标题不能少于两个字符',
            'description.required' => '描述不能为空',
            'description.min' => '描述不能少于四个字符',
            'htmlContent.required' => '文章内容不能为空',
            'htmlContent.min' => '文章内容不能少于四个字符',
            'tags.required' => '标签描述不能为空',
            'tags.min' => '标签描述不能少于两个字符',
        ];
        // 验证数据
        if ($request['typeValue'] !== 'article' && $request['fileDowload'] === 'link') {
            $rule = array_merge($rule, [
                'link' => 'required',
            ]);
            $customInformation = array_merge($customInformation, [
                'link.required' => '文件链接不能为空'
            ]);
        } else if ($request['fileDowload'] === 'upload') {
            $rule = array_merge($rule, [
                'file' => 'required',
            ]);
            $customInformation = array_merge($customInformation, [
                'file.required' => '请上传文件'
            ]);
        }
        $validator = Validator::make($inputArr, $rule, $customInformation);
        return $validator;
    }
    // 获取文章列表
    public function index(Request $request) {
        if (!$request['index'] || !$request['size']) {
            return response()->json([
                'code' => '400400',
                'error' => '未找到页码和分页数'],
                  400,
                 ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        $index = ($request['index'] - 1) * $request['size'];

        $searchData = Article::where([
            "user_id" => Auth::user()->id,
            'delete'=> 0
            ])->skip($index)->take($request['size'])->get();
        if ($searchData) {
            $listArr = array();
            foreach ($searchData as $number => $value) {
                $listArr[$number]['articleId'] = $value['id'];
                $listArr[$number]['alpha'] = $value['alpha'];
                $listArr[$number]['imageUrl'] = $this->getImage($value['id'])->first()['image_url'];
                $listArr[$number]['title'] = $value['title'];
                $listArr[$number]['description'] = $value['description'];
                $listArr[$number]['tags'] = $value['tags'];
                $listArr[$number]['typeValue'] = $value['type_value'];
                $listArr[$number]['createdAt'] = $value['created_at'];
            }

            return response()->json([
                'message' => '获取成功',
                'code' => '200200',
                'total' => Article::where([
                    "user_id" => Auth::user()->id,
                    'delete'=> 0
                    ])->count(),
                'data' => $listArr,], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'error' => '查询失败,请稍后重试',
            'code' => '400400',
            'data' => null,
            ], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    // 获取文章内容
    public function show($alpha) {
        // 多条件查询
        $searchData = Article::where([
            'alpha'=> $alpha,
            'delete'=> 0
            ])->first();
        if ($searchData) {
            return response()->json([
                'message' => '获取成功',
                'code' => '200200',
                'data' => [
                    'articleId' => $searchData['id'],
                    'description' => $searchData['description'],
                    'htmlContent' => $searchData['html_content'],
                    'tags' => $searchData['tags'],
                    'title' => $searchData['title'],
                    'imageData' => $this->getImage($searchData['id'])->get(),
                    'typeValue' => $searchData['type_value'],
                    'link' => $searchData['link'],
                    'file' => $searchData['file'],
                    'alpha' => $searchData['alpha'],
                    'slider' => $searchData['slider'],
                    'movie' => $searchData['movie'] == 1,
                    'fileDowload' => $searchData['file_dowload'],
                    'createdAt' => $searchData['created_at'],
            ]], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'error' => '查询失败,请稍后重试',
            'code' => '400400',
            'data' => null,
            ], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    // 上传图片
    public function uploadimage(Request $request) {
        $validatedData = $request->validate([
            'image' => 'mimes:jpeg,bmp,png,gif,jpg,svg',
        ]);
        $file = $request->file('image');
        if($validatedData){
            $extension =  $file->clientExtension();
            $path = $file->storeAs('public/images', Str::random(32, 'alpha').'.'.$extension);
            return response()->json([
                'code' => 200200,
                'url' => str_replace('public/images/', '', $path)
            ]);
        }
    }
    // 上传字体
    public function fontUpload(Request $request) {
        $file = $request->file('file');
        if($file)
        {
            $extension =  $file->clientExtension();
            $path = $file->storeAs('public/font', Str::random(6, 'alpha').'.'.$extension);
            return response()->json([
                'code' => 200200,
                'url' => str_replace('public/font/', '', $path),
            ]);
        }
    }
    // 上传文件
    public function uploadfile(Request $request) {
        $file = $request->file('file');
        if($file)
        {
            $extension =  $file->clientExtension();
            $path = $file->storeAs('public/file', Str::random(32, 'alpha').'.'.$extension);
            return response()->json([
                'code' => 200200,
                'url' => str_replace('public/file/', '', $path)
            ]);
        }
    }
    // 新增文章
    public function store (Request $request) {
        $inputArr = $request->input();
        if ($this->validator($inputArr, $request)->fails()) {
            foreach ($this->validator($inputArr, $request)->errors()->all() as $msg) {
                return response()->json(['error' => $msg, 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
        };
        // 将链接转换成数组
        $imageData = explode(',', $request['images']);
        $create = Article::create([
            "description" => $request["description"],
            "user_id" => Auth::user()->id,
            "title" => $request["title"],
            "html_content" => $request["htmlContent"],
            "file_dowload" => $request["fileDowload"],
            "link" => $request["link"] ? $request["link"] : '',
            "file" => $request["file"] ? $request["file"] : '',
            "tags" => $request["tags"],
            "type_value" => $request["typeValue"],
            "alpha" => Str::random(15, 'alpha'),
            "created_at" => date('Y-m-d H:i:s'),
            "slider" => $request["slider"],
            "movie" => $request['movie'],
            "browsing" => 0,
            "delete" => 0,
        ]);
        if ($create['id'] && $this->saveImage($imageData, $create['id'], 'add', $request["typeValue"])) {
            return response()->json([
                'message' => '新增内容成功',
                'article_id' => $create['alpha'],
                'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json(['error' => '出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
    // 删除内容
    public function destroy($article_id) {
        $update = Article::where([
            'user_id' => Auth::user()->id,
            'alpha'=> $article_id
            ])->update([
                'delete' => 1
            ]);
            if ($update) {
                return response()->json([
                    'message' => '删除内容成功',
                    'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            } else {
                return response()->json(['error' => '出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
    }
    // 更新内容
    public function update(Request $request, $alpha) {
        $inputArr = $request->input();
        if ($this->validator($inputArr, $request)->fails()) {
            foreach ($this->validator($inputArr, $request)->errors()->all() as $msg) {
                return response()->json(['error' => $msg, 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
        };

        // return($alpha);
        // 将链接转换成数组
        $imageData = explode(',', $request['images']);
        $update = Article::where([
            'user_id' => Auth::user()->id,
            'alpha'=> $alpha
            ])->update([
                "description" => $request["description"],
                "title" => $request["title"],
                "html_content" => $request["htmlContent"],
                "link" => $request["link"] ? $request["link"] : '',
                "file" => $request["file"] ? $request["file"] : '',
                "tags" => $request["tags"],
                "movie" => $request["movie"],
                "slider" => $request["slider"],
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            if ($update && $this->saveImage($imageData, $request['articleId'], 'update', $request['typeValue'])) {
                return response()->json([
                    'message' => '更新内容成功',
                    'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            } else {
                return response()->json(['error' => '出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
    }
    // 删除文件
    public function deleteFile(Request $request, $article_id) {
        if ($request['type'] == 'image') {
            $delete = Images::where([
                'id' => $request['imageId'],
                'article_id'=> $article_id
                ])->delete();
        if ($delete) {
            Storage::delete(['public/images/'.$request['name']]);
            return response()->json([
                'message' => '删除图片成功',
                'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json(['error' => '删除文件出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        } else if ($request['type'] == 'file') {
            $deleteFile =Article::where([
                'id'=> $article_id
                ])->update(['file' => '']);
                if ($deleteFile) {
                    Storage::delete(['public/file/'.$request['name']]);
                    return response()->json([
                        'message' => '删除文件成功',
                        'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
                } else {
                    return response()->json(['error' => '删除文件出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
                }
        }
    }
}
