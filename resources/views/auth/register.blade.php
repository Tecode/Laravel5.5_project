@extends('auth.common')
@section('title', '注册')

@section('content')
<form class="card" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
                <div class="card-body p-6">
                  <div class="card-title">注册账号</div>
                  <div class="form-group">
                    <label class="form-label">昵称</label>
                    <input type="text" name="name" value="{{ old('name') }}" autofocus class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" placeholder="昵称" required>
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                  </div>
                  <div class="form-group">
                    <label class="form-label">邮箱</label>
                    <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" placeholder="邮箱" required>
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                  </div>
                  <div class="form-group">
                    <label class="form-label {{$errors->has('password') ? 'is-invalid' : ''}}">密码</label>
                    <input type="password" name="password" class="form-control" placeholder="密码">
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                  </div>
                  <div class="form-group">
                    <label class="form-label">确认密码</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="确认密码" required>
                  </div>
                  <!-- <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" />
                      <span class="custom-control-label">同意 <a href="terms.html">用户协议</a></span>
                    </label>
                  </div> -->
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">注册</button>
                  </div>
                </div>
              </form>
              <div class="text-center text-muted">
                已有账号? <a href="/login">登录</a>
              </div>
@endsection
