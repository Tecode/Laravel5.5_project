<?php

namespace App\Http\Controllers\Client;

use App\Http\Model\Artifact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class ArtifactController extends Controller
{
    public function artifactList(Request $request)
    {
        return view('front.zbsq_list', [
            'artifactData' => Artifact::where(['delete' => 0])
                ->where('title', 'like', '%' . $request['keywords'] . '%')
                ->paginate(20),
            'keywords' => $request['keywords']
        ]);
    }

    public function artifactDetail(Request $request, $artifact_id)
    {
        Artifact::where('id', $artifact_id)->increment('browsing', 1);
        $requestData = array_values($request->except(['_token']));
//        dd($requestData['txt0']);
//        $dateArray = array(date('Y'), date('m'), date('d'));
        $data = Artifact::where('id', $artifact_id)->first();
        $xAxisArray = explode(' ', $data['xAxis']);
        $yAxisArray = explode(' ', $data['yAxis']);
        $artifact_textArray = sizeof($requestData) > 0 ? $requestData : explode(' ', $data['artifact_text']);
        $font_nameArray = explode(' ', $data['font_name']);
        $font_colorArray = explode(' ', $data['font_color']);
        $font_sizeArray = explode(' ', $data['font_size']);
        $verticalArray = explode(' ', $data['vertical']);
        $horizontalArray = explode(' ', $data['horizontal']);
        $rotationArray = explode(' ', $data['rotation']);
        $autoTextArray = explode(' ', $data['auto_text']);
        $image = Image::make("../public/storage/images/" . $data['image']);
        $arrayLength = sizeof($xAxisArray) - 1;
        if ($data['date'] == '1') {
            $map = [
                'YYYY' => date('Y'),
                'MM' => date('m'),
                'DD' => date('d'),
                'HH' => date('H'),
                'II' => date('i'),
                'SS' => date('s'),
            ];
            foreach ($autoTextArray as $index => $auto_text) {
                $image->text($map[$auto_text] ? $map[$auto_text] : $auto_text, $xAxisArray[$index + $arrayLength], $yAxisArray[$index + $arrayLength], function ($font) use ($index, $font_nameArray, $font_colorArray, $font_sizeArray, $verticalArray, $horizontalArray, $rotationArray, $arrayLength) {
                    $font->file('../public/storage/font/' . $font_nameArray[$index + $arrayLength]);
                    $font->size($font_sizeArray[$index + $arrayLength]);
                    $font->color($font_colorArray[$index + $arrayLength]);
                    $font->align($horizontalArray[$index + $arrayLength]);
                    $font->valign($verticalArray[$index + $arrayLength]);
                    $font->angle($rotationArray[$index + $arrayLength]);
                });
            }
        }
        foreach ($artifact_textArray as $index => $artifact_text) {
            // 闭包
            if ($index < sizeof($artifact_textArray)) {
                $image->text($artifact_textArray[$index], $xAxisArray[$index], $yAxisArray[$index], function ($font) use ($index, $font_nameArray, $font_colorArray, $font_sizeArray, $verticalArray, $horizontalArray, $rotationArray) {
                    $font->file('../public/storage/font/' . $font_nameArray[$index]);
                    $font->size($font_sizeArray[$index]);
                    $font->color($font_colorArray[$index]);
                    $font->align($horizontalArray[$index]);
                    $font->valign($verticalArray[$index]);
                    $font->angle($rotationArray[$index]);
                });
            }
        }
//        $image->text('The quick brown fox jumps over the lazy dog.', 120, 100);
        $alpha = Str::random(8, 'alpha');
        $extension = $image->extension;
        $image->save("../public/storage/zbsq/images" . $alpha . '.' . $extension);
        return view('front.zbsq', [
            'data' => Artifact::where('id', $artifact_id)->first(),
            'image' => '/storage/zbsq/images' . $alpha . '.' . $extension
        ]);
    }

    public function testArtifact()
    {
// pass calls to image cache
        $imgCache = Image::cache(function($images) {
// create Image from file
            $image = $images->make('../public/storage/font/baRrLs.jpeg');
// use callback to define details
            $image->text('foo', 100, 20, function ($font) {
                $font->file('../public/storage/font/zcool.ttf');
                $font->size(24);
                $font->color('#000');
                $font->align('left');
                $font->valign('top');
                $font->angle(0);
            });
        }, 1, true);
        $alpha = Str::random(8, 'alpha');
        $extension = $imgCache->extension;
        $imgCache->save("../storage/app/public/zbsq/images/" . $alpha . '.' . $extension);
        return 'http://localhost:8000/storage/app/public/zbsq/images/'.$alpha . '.' . $extension;
    }
}
