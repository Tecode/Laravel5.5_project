<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class FreePsd extends Model
{
   protected $table = 'psd_info';
   public $timestamps = false;
}
