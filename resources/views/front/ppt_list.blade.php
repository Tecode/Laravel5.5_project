@extends('front.app')
@push('meta')
    <meta name="keywords" content="个人博客,阿明的个人博客,个人博客模板,阿明,免费教程，源码分享，素材，图片">
    <meta name="description" content="阿明的个人博客,是一个会写代码的美工,网站提供免费的教学资源以及网站源码分享，欢迎各位小伙伴来网站留言互动。">
@endpush
@push('script')
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5513119698507366",
            enable_page_level_ads: true
        });
    </script>
@endpush
@section('title', 'html模板 ')
@section('content')
    <div class="row row-cards row-deck">
        @foreach($pptData as $item)
            <div class="col-sm-6 col-xl-3">
                <div class="card">
                    <a href="{{asset('/ppt_template/'.$item->id)}}"><img class="card-img-top"
                                                                         src="{{asset('/storage/images/'.$item->image_url)}}"
                                                                         alt="{{$item->title}}"></a>
                    <div class="card-body d-flex flex-column">
                        <h4><a href="{{asset('/ppt_template/'.$item->id)}}">{{ $item->title }}</a></h4>
                        <div class="text-muted">
                            {{ $item->description }}
                        </div>
                        <div class="d-flex align-items-center pt-5 mt-auto">
                            <div class="avatar avatar-md mr-3"
                                 style="background-image: url(./demo/faces/female/18.jpg)"></div>
                            <div>
                                <a href="./profile.html" class="text-default">{{$item->name}}</a>
                                <small class="d-block text-muted">{{$item->created_at}}</small>
                            </div>
                            <div class="ml-auto text-muted">
                                <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                    <i class="fe fe-eye mr-1"></i>{{ $item->browsing }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="pagination_box">
            {{ $pptData->appends(['keywords' => $keywords])->links() }}
        </div>
    </div>
@endsection
