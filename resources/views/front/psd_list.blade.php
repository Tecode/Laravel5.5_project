@extends('front.app')

@section('title', '免费设计素材下载的网站_免费设计图片素材中国')
@push('meta')
<meta name="keywords" content="素材,图库,图片,图片下载,设计素材,PSD,矢量,AI,CDR,EPS,设计,免费素材网,素材天下,PS素材, Html">
<meta name="description" content="提供矢量图素材,矢量背景图片,矢量图库,还有psd素材,PS素材,设计模板,设计素材,PPT素材,以及网页素材,网站素材,网页图标免费下载">
@endpush
@push('link')
<style>
*,
*::after,
*::before {
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}

body {
	/* background: #fafafa; */
	overflow-x: hidden;
	-webkit-font-smoothing: antialiased;
}

.js .loading::before,
.js .loading::after {
	content: '';
    position: fixed;
	z-index: 1000;
}

.loading::before {
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fafafa;
}

.loading::after {
	top: 50%;
	left: 50%;
	width: 40px;
	height: 40px;
	margin: -20px 0 0 -20px;
	border: 8px solid #383a41;
	border-bottom-color: #565963;
	border-radius: 50%;
	animation: animLoader 0.8s linear infinite forwards;
}

@keyframes animLoader {
	to { transform: rotate(360deg); }
}

a {
	text-decoration: none;
	color: #f2f2f2;
	outline: none;
}

a:hover,
a:focus {
	color: #e6629a;
}

.hidden {
	position: absolute;
	overflow: hidden;
	width: 0;
	height: 0;
	pointer-events: none;
}

main {
	display: flex;
	flex-wrap: wrap;
}

/* Icons */
.icon {
	display: block;
	width: 1.5em;
	height: 1.5em;
	margin: 0 auto;
	fill: currentColor;
}

.content--side {
	position: relative;
	z-index: 100;
	width: 15vw;
	min-width: 130px;
	max-height: 100vh;
	padding: 0 1em;
	order: 2;
}

.content--center {
	flex: 1;
	max-width: calc(100vw - 260px);
	order: 3;
}

.content--right {
	order: 4;
}

.content--related {
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	width: 100%;
	padding: 8em 1em 3em;
	text-align: center;
	order: 5;
}

.media-related {
	width: 100%;
}

.media-item {
	padding: 1em;
}

.media-item__img {
	max-width: 100%;
	opacity: 0.7;
	transition: opacity 0.3s;
}

.media-item:hover .media-item__img,
.media-item:focus .media-item__img {
	opacity: 1;
}

.media-item__title {
	font-size: 1em;
	max-width: 220px;
	padding: 0.5em;
	margin: 0 auto;
}

/* Header */
.codrops-header {
	position: relative;
	z-index: 100;
	display: flex;
	align-items: center;
	width: 100%;
	padding: 3em 1em 4em;
	order: 1;
}

.codrops-header__title {
	font-size: 1em;
	font-weight: normal;
	flex: 1;
	margin: 0 7em 0 0;
	text-align: center;
	text-transform: lowercase;
}

.codrops-header__title::before,
.codrops-header__title::after {
	font-size: 22px;
	font-weight: bold;
	display: inline-block;
	padding: 0 0.25em;
	color: #42454c;
}

.codrops-header__title::after {
	content: '\2309';
	vertical-align: sub;
}

.codrops-header__title::before {
	content: '\230A';
}

/* GitHub corner */
.github-corner {
	position: absolute;
	top: 0;
	right: 0;
}

.github-corner__svg {
	fill: #82888a;
	color: #2c2d31;
	position: absolute; 
	top: 0; 
	border: 0; 
	right: 0;
}

.github-corner:hover .octo-arm {
	animation: octocat-wave 560ms ease-in-out;
}

@keyframes octocat-wave {
	0%,
	100% {
		transform: rotate(0);
	}
	20%,
	60% {
		transform: rotate(-25deg);
	}
	40%,
	80% {
		transform: rotate(10deg);
	}
}

@media (max-width:500px) {
	.github-corner:hover .octo-arm {
		animation: none;
	}
	.github-corner .octo-arm {
		animation: octocat-wave 560ms ease-in-out;
	}
}


/* Top Navigation Style */
.codrops-links {
	position: relative;
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 2.75em;
	margin: 0 0 0 2.25em;
	text-align: center;
	white-space: nowrap;
	background: #1f2125;
}

.codrops-links::after {
	content: '';
	position: absolute;
	top: -10%;
	left: calc(50% - 1px);
	width: 2px;
	height: 120%;
	background: #fafafa;
	transform: rotate3d(0,0,1,22.5deg);
}

.codrops-icon {
	display: inline-block;
	padding: 0 0.65em;
}

/* Controls */
.control--grids {
	margin: 0 0 2.5em;
	text-align: right;
}

.control__title {
	font-size: 0.85em;
	display: block;
	width: 100%;
	margin: 0 0 1em;
	color: #e6629a;
}

.control__item {
	position: relative;
	display: block;
	margin: 0 0 0.5em;
}

.control__radio {
	position: absolute;
	z-index: 10;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	cursor: pointer;
	opacity: 0;
}

.control__label {
	white-space: nowrap;
}

.control__radio:checked + .control__label {
	color: #fff;
	background: #673ab7;
}

.control__radio:not(:checked):hover + .control__label,
.control__btn:hover {
	color: white;
}

.control__btn {
	display: block;
	width: 100%;
	margin: 0 0 0.5em;
	padding: 0;
	text-align: left;
	color: inherit;
	border: none;
	background: none;
}

.control__btn:focus {
	outline: none;
}

/* Grid */

.grid {
	position: relative;
	z-index: 2;
	display: block;
	margin: 0 auto;
}

.grid--hidden {
	position: fixed !important;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	pointer-events: none;
	opacity: 0;
}

.js .grid--loading::before,
.js .grid--loading::after {
	content: '';
	z-index: 1000;
}

.js .grid--loading::before {
	position: fixed;
	top: 0;
	left: 0;
	width: 100vw;
	height: 100vh;
	background: #fafafa;
}

.js .grid--loading::after {
	position: absolute;
	top: calc(25vh - 20px);
	left: 50%;
	width: 40px;
	height: 40px;
	margin: 0 0 0 -20px;
	border: 8px solid #383a41;
	border-bottom-color: #565963;
	border-radius: 50%;
	animation: animLoader 0.8s linear forwards infinite;
}

.grid__sizer {
	margin-bottom: 0 !important;
}

.grid__link,
.grid__img {
	display: block;
}

.grid__img {
	width: 100%;
}

.grid__deco {
	position: absolute;
	top: 0;
	left: 0;
	pointer-events: none;
}

.grid__deco path {
	fill: none;
	stroke: #fff;
	stroke-width: 2px;
}

.grid__reveal {
	position: absolute;
	z-index: 50;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	pointer-events: none;
	opacity: 0;
	background-color: #fafafa;
}
.grid .grid__item {
	box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0,40,100,0.12);
}
.grid .grid__item,
.grid .grid__sizer {
	width: calc(50% - 20px);
	margin: 0 10px 20px;
}
.grid .grid__item h4 {
    text-align: center;
    padding-top: 1em;
    background-color: #f5f5f5;
    padding-bottom: .5em;
}

@media screen and (min-width: 60em) {
	.grid .grid__item,
	.grid .grid__sizer {
		width: calc((100% / 3) - 20px);
		margin: 0 10px 20px;
	}
}

@media screen and (min-width: 70em) {
	.grid .grid__item,
	.grid .grid__sizer {
		width: calc(25% - 30px);
		margin: 0 15px 30px;
	}
}

@media screen and (max-width: 50em) {
	main {
		display: block;
	}
	.codrops-header {
		padding: 1em;
		flex-wrap: wrap;
	}
	.codrops-links {
		margin: 0;
	}
	.codrops-header__title {
		width: 100%;
		text-align: left;
		flex: none;
		margin: 1em 0;
	}
	.content--side {
		width: 100%;
	}
	.content--right {
		order: 3;
	}
	.content--center {
		max-width: 100vw;
	}
	.control {
		margin: 0 0 1em;
		text-align: left;
	}
	.control__item,
	.control__btn {
		display: inline-block;
	}
	.control__btn {
		width: auto;
	}
}

</style>
@endpush
@push('script')
<script>document.documentElement.className = 'js';</script>
<script src="https://cdn.bootcss.com/jquery.imagesloaded/4.1.1/imagesloaded.pkgd.min.js"></script>
<script src="https://cdn.bootcss.com/masonry/4.1.1/masonry.pkgd.min.js"></script>
<script src="{{asset('/assets/js/anime.min.js')}}"></script>
<script src="{{asset('/assets/js/image_loading_main.js')}}"></script>
<!-- /* ------------- 图片延时加载 ------------ */ -->
<!-- <script src="https://cdn.bootcss.com/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>
<script>
	$("img.lazy").lazyload({
		placeholder:"{{asset('/assets/images/loading_img.gif')}}",
		effect:"fadeIn",
	})
</script> -->
<!-- /* ------------- 图片延时加载 ------------ */ -->
@endpush
@push('script')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5513119698507366",
        enable_page_level_ads: true
    });
</script>
@endpush
@section('content')
    <div class="row row-cards row-deck">
        <div id="image_waterfalls_flow" class="col-lg-12 loading">
            <div class="content content--center">
                <div class="grid grid--type-a">
                    <div class="grid__sizer"></div>
                    @foreach($psdData as $item)
                      <div class="grid__item">
                            <a class="grid__link" target="_black" href="{{asset('/psd/'.$item->id)}}">
                                <img class="grid__img lazy" src="{{asset('/storage/images/psd/'.'preview_'.$item->local_url)}}" alt="{{$item->title}}" />
                            </a>
												<div class="card-body d-flex flex-column">
													<h5 style="
													overflow: hidden;
                          text-overflow: ellipsis;
                          display: -webkit-box;
                          -webkit-line-clamp: 1;
                          -webkit-box-orient: vertical;
													"><a href="{{asset('/psd/'.$item->id)}}">{{ $item->title }}</a></h5>
													<div class="text-muted" style="
													overflow: hidden;
                          text-overflow: ellipsis;
                          display: -webkit-box;
                          -webkit-line-clamp: 2;
                          -webkit-box-orient: vertical;
													">
															{{ $item->description }}
													</div>
													<div class="d-flex align-items-center pt-5 mt-auto hidden-xs hidden-sm">
															<div class="ml-auto text-muted">
																	<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
																			<i class="fe fe-eye mr-1"></i>{{ $item->browsing }}</a>
															</div>
													</div>
												</div>
                      </div>
                    @endforeach
								</div>
								<div class="pagination_box">
            			{{ $psdData->appends(['keywords' => $keywords])->links() }}
								</div>
            </div>
        </div>
    </div>
@endsection
