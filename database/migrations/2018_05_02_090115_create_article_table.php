<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('description');
            $table->mediumText('html_content');
            $table->string('tags', 255);
            $table->string('title', 255);
            $table->string('type_value', 50);
            $table->string('browsing', 10);
            $table->string('link', 150);
            $table->string('file', 150);
            $table->unsignedTinyInteger('delete'); // 是否删除
            $table->string('alpha', 20);   //随机字符串
            $table->string('file_dowload', 100); //下载文件是链接还是上传文件
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
