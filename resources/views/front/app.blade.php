<!doctype html>
<html lang="zh" dir="ltr">
<head>
    <meta charset="UTF-8">
    @stack('meta')
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <meta http-equiv="Content-Language" content="zh-cn"/>
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="{{asset('/assets/favicon.ico')}}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/assets/favicon.ico')}}"/>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- 谷歌广告验证 -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5513119698507366",
            enable_page_level_ads: true
        });
    </script>
    <!-- 谷歌广告验证 -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122460879-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-122460879-1');
    </script>
    @stack('script_two')
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?b6693eacab15fc17ab94ccbfecadfa2e";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet"/>
    @stack('link')
</head>
<body class="">
<div class="page">
    <div class="page-main">
        <div class="alert alert-warning" style="text-align: center">
            来领礼包啦！每天均可领，红包更大啦！进[支付]宝搜索词<strong style="color: #f13f40">542337256</strong>领取
        </div>
        <div class="header py-4">
            <div class="d-flex" style="padding: 0 25px;">
                <a class="header-brand" href="/">
                    <img src="{{asset('/demo/brand/tabler.png')}}" class="header-brand-img" alt="tabler logo">
                </a>
                <div class="d-flex order-lg-2 ml-auto">
                    @guest
                    <div class="nav-item d-none d-md-flex">
                        <a href="/login" class="btn btn-sm btn-outline-primary">登录</a>
                    </div>
                    @else
                        <div class="dropdown d-none d-md-flex">
                            <a class="nav-link icon" data-toggle="dropdown">
                                <i class="fe fe-bell"></i>
                                <span class="nav-unread"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a href="#" class="dropdown-item d-flex">
                                    <span class="avatar mr-3 align-self-center"
                                          style="background-image: url(demo/faces/male/41.jpg)"></span>
                                    <div>
                                        <strong>Nathan</strong> pushed new commit: Fix page load performance issue.
                                        <div class="small text-muted">10 minutes ago</div>
                                    </div>
                                </a>
                                <a href="#" class="dropdown-item d-flex">
                                    <span class="avatar mr-3 align-self-center"
                                          style="background-image: url(demo/faces/female/1.jpg)"></span>
                                    <div>
                                        <strong>Alice</strong> started new task: Tabler UI design.
                                        <div class="small text-muted">1 hour ago</div>
                                    </div>
                                </a>
                                <a href="#" class="dropdown-item d-flex">
                                    <span class="avatar mr-3 align-self-center"
                                          style="background-image: url(demo/faces/female/18.jpg)"></span>
                                    <div>
                                        <strong>Rose</strong> deployed new version of NodeJS REST Api V3
                                        <div class="small text-muted">2 hours ago</div>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item text-center text-muted-dark">Mark all as read</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                                <span class="avatar" style="background-image: url(./demo/faces/female/25.jpg)"></span>
                                <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">{{Auth::user()->name}}</span>
                      <small class="text-muted d-block mt-1">Administrator</small>
                    </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="#">
                                    <i class="dropdown-icon fe fe-user"></i> 概况
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="dropdown-icon fe fe-settings"></i> 设置
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="float-right"><span class="badge badge-primary">6</span></span>
                                    <i class="dropdown-icon fe fe-mail"></i> 信箱
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="dropdown-icon fe fe-help-circle"></i> 需要帮助?
                                </a>
                                <a
                                        href="#"
                                        class="dropdown-item"
                                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                    <i class="dropdown-icon fe fe-log-out"></i> 登出
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                        @endguest
                </div>
                <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
                   data-target="#headerMenuCollapse">
                    <span class="header-toggler-icon"></span>
                </a>
            </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 ml-auto">
                        <form class="input-icon my-3 my-lg-0">
                            <input type="search" name="keywords" class="form-control header-search"
                                   placeholder="关键词搜索，例如“618”" tabindex="1">
                            <div class="input-icon-addon">
                                <i class="fe fe-search"></i>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg order-lg-first">
                        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                            <li class="nav-item">
                                <a href="/"
                                   class="nav-link {{substr_count(url()->full(), '/') == 2 ? 'active' : ''}}"><i
                                            class="fe fe-home"></i> 首页</a>
                            </li>
                            <li class="nav-item">
                                <a href="/html"
                                   class="nav-link {{strstr(url()->current(), 'html') ? 'active' : ''}}">
                                    <i class="fe fe-box"></i> HTML模板</a>
                            </li>
                            <li class="nav-item">
                                <a href="/ppt_template"
                                   class="nav-link {{strstr(url()->current(), 'ppt_template') ? 'active' : ''}}">
                                    <i class="fe fe-image"></i> PPT模板</a>
                            </li>
                            <li class="nav-item">
                                <a href="/article"
                                   class="nav-link {{strstr(url()->current(), 'article') ? 'active' : ''}}"><i
                                            class="fe fe-file-text"></i> 文章资源</a>
                            </li>
                            <li class="nav-item">
                                <a href="/psd" class="nav-link {{strstr(url()->current(), 'psd') ? 'active' : ''}}"><i
                                            class="fe fe-file-text"></i> 免费PSD素材</a>
                            </li>
                            <li class="nav-item">
                                <a href="/zbsq" class="nav-link {{strstr(url()->current(), 'zbsq') ? 'active' : ''}}"><i
                                            class="fe fe-file-text"></i> 装逼神器</a>
                            </li>
                            <li class="nav-item">
                            <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-box"></i> 更多</a>
                            <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="/movie" class="dropdown-item ">电影|电视剧</a>
                            {{--<a href="./charts.html" class="dropdown-item ">Charts</a>--}}
                            {{--<a href="./pricing-cards.html" class="dropdown-item ">Pricing cards</a>--}}
                            </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3 my-md-5">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
    {{-- <div class="footer">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-8">--}}
    {{--<div class="row">--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<ul class="list-unstyled mb-0">--}}
    {{--<li><a href="#">First link</a></li>--}}
    {{--<li><a href="#">Second link</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<ul class="list-unstyled mb-0">--}}
    {{--<li><a href="#">Third link</a></li>--}}
    {{--<li><a href="#">Fourth link</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<ul class="list-unstyled mb-0">--}}
    {{--<li><a href="#">Fifth link</a></li>--}}
    {{--<li><a href="#">Sixth link</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<ul class="list-unstyled mb-0">--}}
    {{--<li><a href="#">Other link</a></li>--}}
    {{--<li><a href="#">Last link</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-4 mt-4 mt-lg-0">--}}
    {{--Premium and Open Source dashboard template with responsive and high quality UI. For Free!--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <ul class="list-unstyled mb-0">
                                <li><a href="https://tabler.github.io">Tabler 模板</a></li>
                                <li><a href="http://admin.soscoon.com/login" target="_blank">管理员登录</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-md-3">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="https://tongji.baidu.com/web/welcome/ico?s=b6693eacab15fc17ab94ccbfecadfa2e"
                                       target="_blank">网站访问统计</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mt-4 mt-lg-0">
                    网站提供优质和开源的模板，响应式布局和高质量的用户界面。分享一些文档帮助你建站！
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-auto ml-lg-auto">
                    <div class="row align-items-center">
                        <div class="col-auto">
                            <ul class="list-inline list-inline-dots mb-0">
                                <li class="list-inline-item"><a href="/article">Documentation</a></li>
                                {{--<li class="list-inline-item"><a href="#">备案号</a></li>--}}
                            </ul>
                        </div>
                        <div class="col-auto">
                            <a href="#" class="btn btn-outline-primary btn-sm">蜀ICP备16004803号-1</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                    Copyright © 2018 <a href="#">Blog</a>. Theme by <a href="https://codecalm.net" target="_blank">codecalm.net</a>
                    All rights reserved.
                </div>
            </div>
        </div>
    </footer>
</div>
</body>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="{{asset('/assets/js/core.js')}}"></script>
{{--百度自动推送代码--}}
<script>
    (function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>
@stack('script')
</html>