<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'article';
//    protected $primaryKey = 'article_id';
   public $timestamps = false;
   protected $fillable = [
    'description',
    'user_id',
    'title',
    'html_content',
    'file_dowload',
    'link',
    'tags',
    'type_value',
    'alpha',
    'created_at',
    'browsing',
    'delete',
    'file',
    'slider',
];

}
