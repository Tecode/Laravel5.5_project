<?php

namespace App\Http\Controllers\Api;

use App\Http\Model\Artifact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ArtifactController extends Controller
{
    // 验证数据是否正确
    public function validator($inputArr, $request) {
        $rule = [
            'title' => 'required|min:2',
            'artifactText' => 'required|min:2',
            'cover' => 'required|min:2',
            'fontColor'=> 'required|min:2',
            'fontName'=> 'required|min:2',
            'fontSize'=> 'required|min:2',
            'horizontal'=> 'required|min:2',
            'image'=> 'required|min:2',
            'rotation'=> 'required|min:2',
            'vertical'=> 'required|min:2',
            'xAxis'=> 'required|min:2',
            'yAxis'=> 'required|min:2',
        ];
        $customInformation = [
            'title.required' => '标题不能为空',
            'title.min' => '标题不能少于两个字符'
        ];
        $validator = Validator::make($inputArr, $rule, $customInformation);
        return $validator;
    }
    public function store(Request $request) {
        $inputArr = $request->input();
        if ($this->validator($inputArr, $request)->fails()) {
            foreach ($this->validator($inputArr, $request)->errors()->all() as $msg) {
                return response()->json(['error' => $msg, 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
        };
        $insert = Artifact::insert([
            "title" => $request["title"],
            "artifact_text" => $request["artifactText"],
            "date" => $request["date"],
            "font_color" => $request["fontColor"],
            "font_local_name" => $request["fontLocalName"],
            "font_name" => $request["fontName"],
            "font_size" => $request["fontSize"],
            "horizontal" => $request["horizontal"],
            "image" => $request["image"],
            "rotation" => $request["rotation"],
            "upload_font" => $request["uploadFont"],
            "vertical" => $request["vertical"],
            "xAxis" => $request["xAxis"],
            "yAxis" => $request["yAxis"],
            "font_image" => $request["fontImage"],
            "cover" => $request["cover"],
            "auto_text" => $request["autoText"],
            "user_id" => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "browsing" => 0,
            "delete" => 0,
        ]);
        if ($insert) {
            return response()->json([
                'message' => '新增内容成功',
                'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json(['error' => '出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
    // 获取列表
    public function index(Request $request) {
        if (!$request['index'] || !$request['size']) {
            return response()->json([
                'code' => '400400',
                'error' => '未找到页码和分页数'],
                400,
                ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        $index = ($request['index'] - 1) * $request['size'];

        $searchData = Artifact::where([
            "user_id" => Auth::user()->id,
            'delete'=> 0
        ])->skip($index)->take($request['size'])->get();
        if ($searchData) {
            $listArr = array();
            foreach ($searchData as $number => $value) {
                $listArr[$number]['title'] = $value['title'];
                $listArr[$number]['artifactId'] = $value["id"];
                $listArr[$number]['artifactText'] = $value['artifact_text'];
                $listArr[$number]['date'] = $value['date'];
                $listArr[$number]['fontColor'] = $value['font_color'];
                $listArr[$number]['fontLocalName'] = $value['font_local_name'];
                $listArr[$number]['fontName'] = $value['font_name'];
                $listArr[$number]['fontSize'] = $value['font_size'];
                $listArr[$number]['horizontal'] = $value['horizontal'];
                $listArr[$number]['image'] = $value['image'];
                $listArr[$number]['rotation'] = $value['rotation'];
                $listArr[$number]['uploadFont'] = $value['upload_font'];
                $listArr[$number]['vertical'] = $value['vertical'];
                $listArr[$number]['xAxis'] = $value['xAxis'];
                $listArr[$number]['yAxis'] = $value['yAxis'];
                $listArr[$number]['cover'] = $value['cover'];
                $listArr[$number]['autoText'] = $value['auto_text'];
                $listArr[$number]['horizontal'] = $value['horizontal'];
                $listArr[$number]['horizontal'] = $value['horizontal'];
            }

            return response()->json([
                'message' => '获取成功',
                'code' => '200200',
                'total' => Artifact::where([
                    "user_id" => Auth::user()->id,
                    'delete'=> 0
                ])->count(),
                'data' => $listArr,], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'error' => '查询失败,请稍后重试',
            'code' => '400400',
            'data' => null,
        ], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    // 更新内容
    public function update(Request $request, $artifact_id) {
        $inputArr = $request->input();
        if ($this->validator($inputArr, $request)->fails()) {
            foreach ($this->validator($inputArr, $request)->errors()->all() as $msg) {
                return response()->json(['error' => $msg, 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
        };
        $update = Artifact::where([
            'user_id' => Auth::user()->id,
            'id'=> $artifact_id
        ])->update([
            "title" => $request["title"],
            "artifact_text" => $request["artifactText"],
            "date" => $request["date"],
            "font_color" => $request["fontColor"],
            "font_local_name" => $request["fontLocalName"],
            "font_name" => $request["fontName"],
            "font_size" => $request["fontSize"],
            "horizontal" => $request["horizontal"],
            "image" => $request["image"],
            "rotation" => $request["rotation"],
            "upload_font" => $request["uploadFont"],
            "vertical" => $request["vertical"],
            "xAxis" => $request["xAxis"],
            "yAxis" => $request["yAxis"],
            "cover" => $request["cover"],
            "font_image" => $request["fontImage"],
            "auto_text" => $request["autoText"],
            "updated_at" => date('Y-m-d H:i:s'),
        ]);
        if ($update) {
            return response()->json([
                'message' => '更新内容成功',
                'code' => '200200'], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json(['error' => '出错了,请稍后重试', 'code' => '400400'], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
    // 获取文章内容
    public function show($artifact_id) {
        // 多条件查询
        $searchData = Artifact::where([
            'id'=> $artifact_id,
            'delete'=> 0
        ])->first();
        if ($searchData) {
            return response()->json([
                'message' => '获取成功',
                'code' => '200200',
                'data' => [
            'title' => $searchData['title'],
            'artifactId' => $searchData["id"],
            'artifactText' => $searchData['artifact_text'],
            'date' => $searchData['date'],
            'fontColor' => $searchData['font_color'],
            'fontLocalName' => $searchData['font_local_name'],
            'fontName' => $searchData['font_name'],
            'fontSize' => $searchData['font_size'],
            'fontImage' => $searchData['font_image'],
            'horizontal' => $searchData['horizontal'],
            'image' => $searchData['image'],
            'rotation' => $searchData['rotation'],
            'uploadFont' => $searchData['upload_font'],
            'vertical' => $searchData['vertical'],
            'xAxis' => $searchData['xAxis'],
            'yAxis' => $searchData['yAxis'],
            'cover' => $searchData['cover'],
            'autoText' => $searchData['auto_text']
                ]], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'error' => '查询失败,请稍后重试',
            'code' => '400400',
            'data' => null,
        ], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    public function fontList(Request $request) {
        if (!$request['index'] || !$request['size']) {
            return response()->json([
                'code' => '400400',
                'error' => '未找到页码和分页数'],
                400,
                ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        $index = ($request['index'] - 1) * $request['size'];

        $searchData = Artifact::where([
            "user_id" => Auth::user()->id,
            "delete"=> 0
        ])->skip($index)->take($request['size'])->get();
        if ($searchData) {
            $listArr = array();
            foreach ($searchData as $number => $value) {
                if ($value['upload_font']) {
                    $listArr[$number]['artifactId'] = $value['id'];
                    $listArr[$number]['uploadFont'] = $value['upload_font'];
                    $listArr[$number]['fontLocalName'] = $value["font_local_name"];
                }
            }

            return response()->json([
                'message' => '获取成功',
                'code' => '200200',
                'total' => Artifact::where([
                    "user_id" => Auth::user()->id,
                    'delete'=> 0
                ])->count(),
                'data' => $listArr], 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'error' => '查询失败,请稍后重试',
            'code' => '400400',
            'data' => null,
        ], 400, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
