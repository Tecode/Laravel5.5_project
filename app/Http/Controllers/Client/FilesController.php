<?php

namespace App\Http\Controllers\Client;

use App\Http\Model\HtmlTemplate;
use Illuminate\Http\Request;
use App\Http\Model\Article;
use App\Http\Model\Images;
use App\Http\Model\FreePsd;
use Illuminate\Foundation\Auth\User;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function getMax($psd_id)
    {
        return FreePsd::where('delete', '=', 0)->where('id', '<', $psd_id)->max('id');
    }

    public function getMin($psd_id)
    {
        return FreePsd::where('delete', '=', 0)->where('id', '>', $psd_id)->min('id');
    }
    public function createData($data)
    {
        foreach ($data as $dataItem) {
            $dataItem['image_url'] = Images::where('article_id', $dataItem['id'])->first()['image_url'];
            $dataItem['name'] = User::where('id', $dataItem['user_id'])->first()['name'];

        }
        return $data;
    }

    public function filesList(Request $request)
    {
        return view('front.ppt_list', [
            'pptData' => $this->createData(Article::where(['delete' => 0, 'type_value' => 'ppt'])
                ->where('title', 'like', '%'.$request['keywords'].'%')
                ->paginate(12)),
            'keywords' => $request['keywords']
        ]);
    }

    public function htmlList(Request $request)
    {
        return view('front.html_list', [
            'htmlData' => $this->createData(Article::where(['delete' => 0, 'type_value' => 'html'])
                    ->where('title', 'like', '%'.$request['keywords'].'%')
                    ->paginate(12)),
            'keywords' => $request['keywords']
        ]);
    }
    public function psdList(Request $request) {
        return view('front.psd_list', [
            'psdData' => FreePsd::where(['delete' => 0])
                ->where('description', 'like', '%'.$request['keywords'].'%')
                ->paginate(20),
            'keywords' => $request['keywords']
        ]);
    }
    public function psdDetail($psd_id) {
        FreePsd::where('id', $psd_id)->increment('browsing', 1);
        return view('front.psd', [
            'data' => FreePsd::where('id', $psd_id)->first(),
            'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
            'pre' => $this->getMin($psd_id) ? FreePsd::where('id', $this->getMin($psd_id))->first() : null,
            'next' => $this->getMax($psd_id) ? FreePsd::where('id', $this->getMax($psd_id))->first() : null,
            ]);
    }
    public function htmlTemplateList(Request $request) {
        return view('front.html_list', [
            'psdData' => HtmlTemplate::where(['delete' => 0])
                ->where('description', 'like', '%'.$request['keywords'].'%')
                ->paginate(12),
            'keywords' => $request['keywords']
        ]);
    }
    public function htmlTemplateDetail($html_id) {
        HtmlTemplate::where('id', $html_id)->increment('browsing', 1);
        return view('front.html', [
            'data' => HtmlTemplate::where('id', $html_id)->first(),
            'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
            'pre' => $this->getMin($html_id) ? HtmlTemplate::where('id', $this->getMin($html_id))->first() : null,
            'next' => $this->getMax($html_id) ? HtmlTemplate::where('id', $this->getMax($html_id))->first() : null,
        ]);
    }
    // demo预览
    public function htmlTemplatePreview($html_id) {
        HtmlTemplate::where('id', $html_id)->increment('browsing', 1);
        return view('other.demo_preview', [
            'data' => HtmlTemplate::where('id', $html_id)->first()
        ]);
    }
}
