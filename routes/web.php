<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/vue', function () {
    return view('vue');
});

// 客户端路由
Route::group(['namespace' => 'Client', 'prefix' => ''], function () {
    // 首页列表
    Route::get('/', 'ArticleController@index');
    // 查看文章详情
    Route::get('/article/{article_id}', 'ArticleController@detail');
    Route::get('/ppt_template/{article_id}', 'ArticleController@pptDetail');
    Route::get('/html_template/{article_id}', 'ArticleController@fileDetail');
    // 列表
    Route::get('/article', 'ArticleController@articleList');
    Route::get('/ppt_template', 'FilesController@filesList');
//    Route::get('/html_template','FilesController@htmlList');
    // psd
    Route::get('/psd', 'FilesController@psdList');
    Route::get('/psd/{psd_id}', 'FilesController@psdDetail');
    // html
    Route::get('/html', 'FilesController@htmlTemplateList');
    Route::get('/html/{html_id}', 'FilesController@htmlTemplateDetail');
    // 电影电视剧
    Route::get('/movie', 'ArticleController@movieList');
    // demo预览
    Route::get('/html_preview/{html_id}', 'FilesController@htmlTemplatePreview');
    // 装逼神器
    Route::get('/zbsq', 'ArtifactController@artifactList');
    Route::any('/zbsq/{artifact_id}', 'ArtifactController@artifactDetail');
    // 装逼神器调试
    Route::get('/test_artifact', 'ArtifactController@testArtifact');
});
// 发放token
Route::get('/auth/callback', function (Request $request){
    if ($request->get('code')) {
        // $http = new GuzzleHttp\Client;

        // $response = $http->post('http://127.0.0.1:8000/oauth/token', [
        //     'form_params' => [
        //         'grant_type' => 'authorization_code',
        //         'client_id' => '5',
        //         'client_secret' => 'dWFdMxTawqVZLPOaQAOu1yXfRzAKorxSOAne00R8',
        //         'redirect_uri' => 'http://127.0.0.1:8000/auth/callback',
        //         'code' => $request->get('code'),
        //     ],
        // ]);
    
        // return json_decode((string) $response->getBody(), true);
        return 'Access OK';
    } else {
        return 'Access Denied';
    }
});
// 账号密码授权
Route::get('/auth/password', function (Request $request){
    $http = new GuzzleHttp\Client(['timeout'  => 5.0]);
//    $response = $http->request('GET', 'http://127.0.0.1:8000/oauth/token');
    $response = $http->request('POST', 'http://127.0.0.1:8000/oauth/token', [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => '2',
            'client_secret' => 'hbIbslC7bZPvC6wJSkxpt7i10kUsOHEfMhaKenpt',
            'username' => '123@qq.com',
            'password' => '123456',
            'scope' => '',
        ]
    ]);
    dd($response);
//    return json_decode((string)$response->getBody(), true);
});
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('login', 'LoginController@index')->name('admin.login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout');
    Route::get('index', 'LoginController@index');
    Route::get('home', 'IndexController@index');
});
    //登录才可以访问的路由
Route::group(['middleware' => ['web', 'login'], 'namespace' => 'Front', 'prefix' => ''], function () {
    // Route::post('/payOrder/{scenicId}', 'OrderListController@createOrder');
    // Route::get('/orderDetail/{orderId}', 'OrderListController@orderDetail');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('npy', function () {
    return view('other.npy');
});
