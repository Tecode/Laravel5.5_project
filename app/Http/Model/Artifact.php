<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Artifact extends Model
{
    protected $table = 'artifact';
    public $timestamps = false;
}
