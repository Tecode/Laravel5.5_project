@extends('front.app')

@section('title', '文章资源')
@push('meta')
<meta name="keywords" content="文章，技术文档，有趣的分享">
<meta name="description" content="一些有用的资源文档">
@endpush
@push('script')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5513119698507366",
        enable_page_level_ads: true
    });
</script>
@endpush
@section('content')
    <div class="row row-cards row-deck" style="position: static">
        <div class="col-lg-12">
            @foreach($articleData as $item)
                <div class="card">
                    <div class="card-body d-flex flex-column">
                        <h4><a href="{{asset('/article/'.$item->id)}}">{{ $item->title }}</a></h4>
                        <div class="text-muted">
                            {{ $item->description }}
                        </div>
                        <div class="d-flex align-items-center pt-5 mt-auto">
                            <div class="avatar avatar-md mr-3"
                                 style="background-image: url(./demo/faces/female/29.jpg)"></div>
                            <div>
                                <a href="javascript:void(0)" class="text-default">{{ $item->name }}</a>
                                <small class="d-block text-muted">{{ $item->created_at }}</small>
                            </div>
                            <div class="ml-auto text-muted">
                                <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                    <i class="fe fe-eye mr-1"></i>{{ $item->browsing }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="pagination_box">
                {{ $articleData->appends(['keywords' => $keywords])->links() }}
            </div>
        </div>
    </div>
@endsection
