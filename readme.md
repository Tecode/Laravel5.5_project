<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About Laravel

```bash
php artisan make:migration create_articel_table
```

## 数据库迁移

```
php artisan migrate
php artisan migrate:refresh
```
## 数据填充


```
php artisan make:seeder AdminsTableSeeder

$this->call('UserTableSeeder');
$this->call(UserTableSeeder::class);

composer dump-autoload
php artisan migrate --seed
```
## 错误处理
```
Call to undefined function mcrypt_decrypt()

// Ubuntu
sudo apt-get install mcrypt php7.0-mcrypt

// CenOS
sudo yum install mcrypt php7.2-mcrypt
```
```bash
The Response content must be a string or object implementing __toString(), "boolean" given.
Trying to get property of non-object

解决方法 运行
php artisan passport:client --personal
修改 `site_oauth_clients` 表里面的的 `personal_access_client` `password_client`字段

```

```
"storage/oauth-private.key" does not exist or is not readable

php artisan passport:keys
```
```text
oauth/personal-access-tokens报500问题
php artisan passport:client --personal
```
// 上传大文件需要设置php.ini
```bash
sudo find / -name 'php.ini'

post_max_size = 500M;
memory_limit = 500M;
upload_max_filesize = 500M
```