<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{asset('css/app.css')}}" />
  <title>vue-tempte</title>
</head>
<body>
<div id="app">
  <example-component :data={{time()}}></example-component>
  <passport-clients></passport-clients>
  <passport-authorized-clients></passport-authorized-clients>
  <passport-personal-access-tokens></passport-personal-access-tokens>
</div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>