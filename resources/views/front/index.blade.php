@extends('front.app')
@push('meta')
<meta name="keywords" content="个人博客,阿明的个人博客,个人博客模板,阿明,免费教程，源码分享，素材，图片">
<meta name="description" content="阿明的个人博客,是一个会写代码的美工,网站提供免费的教学资源以及网站源码分享，欢迎各位小伙伴来网站留言互动。">
@endpush
@section('title', '乐游网站|站内资源分享，学习心得')
@section('content')
<div class="row">
              <div class="col-lg-3 order-lg-1 mb-4 hidden-xs hidden-sm">
                <a href="#" class="btn btn-block btn-primary mb-6">
                  扫码领红包
                </a>
                <div class="card">
                    <div class="d-flex flex-column">
                        <img src="http://admin.soscoon.com/uploadImages/285b663408cd50e35daface5d61b6395326f32ff.PNG" alt="红包领取" />
                    </div>
                </div>
                <!-- Getting started -->
                <div class="list-group list-group-transparent mb-0">
                  <a class="list-group-item list-group-item-action active"><span class="icon mr-3"><i class="fe fe-flag"></i></span>热门推荐</a>
                </div>
                <!-- Components -->
                <div class="list-group list-group-transparent mb-0">
                    @foreach($hotData as $item)
                        @if ($item->type_value == 'article')
                            <a href="{{asset('/article/'.$item->id)}}" target="_blank" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                        @elseif ($item->type_value == 'ppt')
                            <a href="{{asset('/ppt_template/'.$item->id)}}" target="_blank" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                        @else
                            <a href="{{asset('/html_template/'.$item->id)}}" target="_blank" class="list-group-item list-group-item-action"><span class="icon mr-3"><i class="fa fa-paper-plane-o"></i></span>{{$item->title}}</a>
                        @endif
                    @endforeach
                </div>
              </div>
              <div class="col-lg-9">
              @foreach($articleData as $articleItem)
                <div class="card card-aside" style="overflow: hidden;">
                        @if ($articleItem->type_value == 'article')
                            <a href="{{asset('/article/'.$articleItem->id)}}" target="_blank" class="card-aside-column" style="background-image: url({{asset('/storage/images/'.$articleItem->image_url)}})"></a>
                        @elseif ($articleItem->type_value == 'ppt')
                            <a href="{{asset('/ppt_template/'.$articleItem->id)}}" target="_blank" class="card-aside-column" style="background-image: url({{asset('/storage/images/'.$articleItem->image_url)}})"></a>
                        @else
                            <a href="{{asset('/html_template/'.$articleItem->id)}}"target="_blank" class="card-aside-column" style="background-image: url({{asset('/storage/images/'.$articleItem->image_url)}})"></a>
                        @endif
                    <div class="card-body d-flex flex-column">
                      <h4 style="
                          overflow: hidden;
                          text-overflow: ellipsis;
                          display: -webkit-box;
                          -webkit-line-clamp: 1;
                          -webkit-box-orient: vertical;
                      ">
                        @if ($articleItem->type_value == 'article')
                              <a href="{{asset('/article/'.$articleItem->id)}}" target="_blank">{{ $articleItem->title }}</a>
                        @elseif ($articleItem->type_value == 'ppt')
                              <a href="{{asset('/ppt_template/'.$articleItem->id)}}" target="_blank">{{ $articleItem->title }}</a>
                        @else
                              <a href="{{asset('/html_template/'.$articleItem->id)}}" target="_blank">{{ $articleItem->title }}</a>
                        @endif
                      </h4>
                      <div style="
                          overflow: hidden;
                          text-overflow: ellipsis;
                          display: -webkit-box;
                          -webkit-line-clamp: 2;
                          -webkit-box-orient: vertical;
                      " class="text-muted" title={{$articleItem->description}}>{{ mb_strlen($articleItem->description) > 130 ? str_limit($articleItem->description, $limit = 130, $end = '...') : $articleItem->description }}</div>
                      <div class="d-flex align-items-center pt-5 mt-auto">
                        <div class="avatar avatar-md mr-3" style="background-image: url(./demo/faces/female/18.jpg)"></div>
                        <div>
                          <a href="javascript:void(0)" class="text-default"> {{ $articleItem->name }}</a>
                          <small class="d-block text-muted">{{ $articleItem->created_at }}</small>
                        </div>
                        <div class="ml-auto text-muted">
                          <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-eye mr-1"></i>{{$articleItem->browsing}}</a>
                        </div>
                      </div>
                    </div>
                  </div>
              @endforeach
				<div class="pagination_box">
                  {{ $articleData->appends(['keywords' => $keywords])->links() }}
				</div>
            </div>
          </div>
@endsection
