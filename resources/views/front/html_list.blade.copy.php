@extends('front.app')

@section('title', 'html模板')
@push('meta')
<meta name="keywords" content="文章，技术文档，有趣的分享">
<meta name="description" content="办公包含各个行业精品PPT模板，如工作总结、工作汇报、述职报告、答辩、简历、计划、培训、婚庆等等，下载精美好看的动态PPT模板就到熊猫办公PPT">
@endpush
@push('script')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5513119698507366",
        enable_page_level_ads: true
    });
</script>
@endpush
@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Gallery
        </h1>
        <div class="page-subtitle">1 - 12 of 1713 photos</div>
        <div class="page-options d-flex">
            <select class="form-control custom-select w-auto">
                <option value="asc">Newest</option>
                <option value="desc">Oldest</option>
            </select>
            <div class="input-icon ml-2">
                  <span class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </span>
                <input type="text" class="form-control w-10" placeholder="Search photo">
            </div>
        </div>
    </div>
    <div class="row row-cards">
        @foreach($htmlData as $item)
            <div class="col-sm-6 col-lg-4">
                <div class="card p-3">
                    <a href="{{asset('/html_template/'.$item->id)}}" class="mb-3">
                        <img src="{{asset('/storage/images/'.$item->image_url)}}" alt="{{$item->title}}"
                             class="rounded">
                    </a>
                    <div class="d-flex align-items-center px-2">
                        <div class="avatar avatar-md mr-3" style="background-image: url(demo/faces/male/41.jpg)"></div>
                        <div>
                            <div>{{ $item->name }}</div>
                            <small class="d-block text-muted">{{ $item->created_at }}</small>
                        </div>
                        <div class="ml-auto text-muted">
                            {{--<a href="javascript:void(0)" class="icon"><i class="fe fe-eye mr-1"></i> 112</a>--}}
                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i
                                        class="fe fe-eye mr-1"></i> {{$item->browsing}}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="pagination_box">
            {{ $htmlData->appends(['keywords' => $keywords])->links() }}
		</div>
    </div>
@endsection