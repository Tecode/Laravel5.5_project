<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/userInfo', function () {
    $map = [
        "0" => false,
        "1" => true,
    ];
    return response()->json([
        'code' => '200200', 'data' => [
            'userId' => Auth::user()['id'],
            'artifact' => $map[Auth::user()['artifact']],
            'admin' => $map[Auth::user()['admin']],
            'name' => Auth::user()['name'],
            'email' => Auth::user()['email']
        ]], 200,
        ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
})->middleware('auth:api');
// api相关的路由，登录了有token才可以访问
Route::group(['middleware' => ['auth:api'], 'namespace' => 'Api'], function () {
    // 图片文件上传
    Route::post('/uploadimage', 'ArticleController@uploadimage');
    Route::post('/uploadfile', 'ArticleController@uploadfile');
    Route::post('/fontupload', 'ArticleController@fontUpload');
    Route::delete('/file/{article_id}', 'ArticleController@deleteFile');
    // 文章相关的所有路由
    Route::resource('/article', 'ArticleController');
    // 装逼神器
    Route::resource('/artifact', 'ArtifactController');
    // 字体列表
    Route::get('/font_list', 'ArtifactController@fontList');
});
// 登录
Route::post('/login', function (Request $request) {
    // 将编码的密码转码
    $key = pack("H*", "0123456789abcdef0123456789abcdef");
    $iv = pack("H*", "abcdef9876543210abcdef9876543210");
    //Now we receive the encrypted from the post, we should decode it from base64,
    $shown = openssl_decrypt(base64_decode($request['password']), "AES-128-CBC", $key, OPENSSL_RAW_DATA, $iv);

    $loginData = $request;
    // 解码以后会出现/50 /0F 的字符要替换
    // admin为1说明有权限登录后台
    $loginData['password'] = trim(preg_replace('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/', '', $shown));
    if (Auth::attempt([
        'email' => $request['email'],
        'password' => $loginData['password'],
        'admin' => 1
    ])) {
        return [
            'token' => Auth::user()->createToken('access_token')->accessToken,
            'token_type' => 'Bearer'
        ];
    }
    return response()->json([
        'code' => '404404', 'error' => '账号密码错误'], 404,
        ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
});

//认证API /api/redirect
Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '5',
        'redirect_uri' => 'http://127.0.0.1:8000/auth/callback',
        'response_type' => 'code',
        'scope' => 'place-orders check-status',
    ]);

    return redirect('http://127.0.0.1:8000/oauth/authorize?' . $query);
});