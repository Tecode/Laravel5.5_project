<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\Images;
use App\Http\Model\Article;
use App\User;

class ArticleController extends Controller
{
    public function getMax($article_id)
    {
        return Article::where('delete', '=', 0)->where('id', '<', $article_id)->max('id');
    }

    public function getMin($article_id)
    {
        return Article::where('delete', '=', 0)->where('id', '>', $article_id)->min('id');
    }

    public function createData($data)
    {
        foreach ($data as $dataItem) {
            $dataItem['image_url'] = Images::where('article_id', $dataItem['id'])->first()['image_url'];
            $dataItem['name'] = User::where('id', $dataItem['user_id'])->first()['name'];

        }
        return $data;
    }

    public function index(Request $request)
    {
        return view('front.index', [
                'articleData' => $this->createData(Article::orderBy('created_at', 'desc')
                    ->where(['delete' => 0, 'movie' => '0'])
                    ->where('title', 'like', '%'.$request['keywords'].'%')
                    ->paginate(20)),
                'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
                'keywords' => $request['keywords']
            ]
        );
    }

    public function detail($article_id)
    {
        Article::where('id', $article_id)->increment('browsing', 1);
        return view('front.article', [
            'data' => Article::where('id', $article_id)->first(),
            'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
            'pre' => $this->getMin($article_id) ? Article::where('id', $this->getMin($article_id))->first() : null,
            'next' => $this->getMax($article_id) ? Article::where('id', $this->getMax($article_id))->first() : null,
        ]);
    }

    public function pptDetail($article_id)
    {
        Article::where('id', $article_id)->increment('browsing', 1);
        return view('front.ppt', [
            'data' => Article::where('id', $article_id)->first(),
            'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
            'images' => Images::where('article_id', $article_id)->get(),
            'pre' => $this->getMin($article_id) ? Article::where('id', $this->getMin($article_id))->first() : null,
            'next' => $this->getMax($article_id) ? Article::where('id', $this->getMax($article_id))->first() : null,
        ]);
    }

    public function fileDetail($article_id)
    {
        Article::where('id', $article_id)->increment('browsing', 1);
        return view('front.article', [
            'data' => Article::where('id', $article_id)->first(),
            'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
            'pre' => $this->getMin($article_id) ? Article::where('id', $this->getMin($article_id))->first() : null,
            'next' => $this->getMax($article_id) ? Article::where('id', $this->getMax($article_id))->first() : null,
        ]);
    }

    public function articleList(Request $request)
    {
        return view('front.article_list', [
            'articleData' => $this->createData(Article::where(['delete' => 0, 'type_value' => 'article'])
                ->where('title', 'like', '%'.$request['keywords'].'%')
                ->paginate(20)),
            'keywords' => $request['keywords']
        ]);
    }

    public function movieList(Request $request)
    {
        return view('front.movie_list', [
                'movieData' => $this->createData(Article::orderBy('created_at', 'desc')
                    ->where(['delete' => 0, 'movie' => '1'])
                    ->where('title', 'like', '%'.$request['keywords'].'%')
                    ->paginate(20)),
                'hotData' => Article::orderBy('browsing', 'desc')->take(10)->get(),
                'keywords' => $request['keywords']
            ]
        );
    }
}
